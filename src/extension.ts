// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    console.log("start loading vscode-x")
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    //console.log('Congratulations, your extension "vscode-x" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('vscode-x.helloWorld', () => {
    	// The code you place here will be executed every time your command is executed
        console.log("on command: vscode-x.helloWorld");
    	// Display a message box to the user
    	vscode.window.showInformationMessage('Hello World from vscode-x!');
    	
    });
    context.subscriptions.push(disposable);

    //"command": "vscode-x.hello",  vscode-x必须和插件名字一样，猜测这个是用来做名字空间管理的
    //"title": "vsx.hello",         这些会出现在命令窗口上
    //"description": "vscode-x: hello world !"
    let cmd = vscode.commands.registerCommand('vscode-x.hello', () => {
        console.log("on command: vscode-x.hello");
        //vscode.window.showInformationMessage('on command: vscode-x.hello!');
    });
    context.subscriptions.push(cmd);

    // 这样好像无法注册
    //context.subscriptions.concat(cmds);

    //context.subscriptions.push(cmds);

    // registerTextEditorCommand
    let txtCmd = vscode.commands.registerCommand("vscode-x.python: insert file statement",
        () => {
            console.log("on command: vscode-x.py");
        });
        console.log(txtCmd)
    context.subscriptions.push(txtCmd);

    console.log("finish loading vscode-x")
}

// 调试日志
//console.log('vscode-x is online !');

// 左下角弹框显示
//vscode.window.showInformationMessage('Hello World from vscode-x!');

// this method is called when your extension is deactivated
export function deactivate() {}
